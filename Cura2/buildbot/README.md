# Cura2 BuildBot Configuration

## Installation Instructions

1. Copy master.cfg and config.json into the buildbot Cura2 master directory.
2. Modify the passwords in config.json.